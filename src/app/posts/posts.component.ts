import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  [x: string]: any;

  userId;
  name;
  Id;
  title;
  body;
  postsData$;

  


  constructor(private route: ActivatedRoute, private postsService:PostsService)
  {
  }

  ngOnInit() {
    this.postsData$ = this.postsService.searchPostsData();
    console.log(this.postsData$);
    
  }

}
