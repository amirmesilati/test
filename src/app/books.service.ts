import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { tap, catchError, map} from 'rxjs/operators';

 

@Injectable({
  providedIn: 'root'
})
export class BooksService {
 
  //books: any =  [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}]

  userCollection:AngularFirestoreCollection = this.db.collection('users');
  bookCollection:AngularFirestoreCollection;



  /*
  addBooks(){
    setInterval(() => 
      this.books.push({title:'A new one', author:'New author'})
  , 5000);    
  }
  */

 
  /*
  getBooks(): any {
    const booksObservable = new Observable(observer => {
           setInterval(() => 
               observer.next(this.books)
           , 5000);
    });  
    return booksObservable;
  }
  */

  getBooks(userId): Observable<any[]> {
    //const ref = this.db.collection('books');
    //return ref.valueChanges({idField: 'id'});
    this.bookCollection = this.db.collection(`users/${userId}/books`);//USING BACKTICK- LEFT TO NUMBER 1 ON KEYBOARD
    console.log('Books collection created');
    return this.bookCollection.snapshotChanges().pipe(
      map(collection => collection.map(document => {
        const data = document.payload.doc.data();
        data.id = document.payload.doc.id;
        console.log(data);
        return data;
      }))
    );    
  } 

  getBook(userId, id:string):Observable<any>{
    return this.db.doc(`users/${userId}/books/${id}`).get();
  }
  
  addBook(userId:string, title:string, author:string){
    console.log('In add books');
    const book = {title:title,author:author}
    //this.db.collection('books').add(book)  
    this.userCollection.doc(userId).collection('books').add(book);//THE FIREBASE DATABASE
  } 

  updateBook(userId:string, id:string,title:string,author:string){
    this.db.doc(`users/${userId}/books/${id}`).update(
      {
        title:title,
        author:author
      }
    )
  }
  
  deleteBook(userId:string, id:string){
    this.db.doc(`users/${userId}/books/${id}`).delete();
  }

  constructor(private db: AngularFirestore,
              private authService:AuthService) {}
}
