import { Posts } from './interfaces/posts';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class PostsService{

  private URL="https://jsonplaceholder.typicode.com/posts";

  constructor(private http:HttpClient) {}


  searchPostsData():Observable<Posts>
  {
    console.log(this.http.get<Posts>(`${this.URL}`));
    return this.http.get<Posts>(`${this.URL}`);
  }
  
  
}
